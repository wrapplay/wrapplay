# WRAPPLAY

---

<!-- TOC -->
* [WRAPPLAY](#wrapplay)
  * [Features](#features)
  * [Quick start](#quick-start)
  * [INSTALLATION](#installation)
    * [ENVIRONMNET : DEV](#environmnet--dev)
    * [ENVIRONMNET : PRODUCTION](#environmnet--production)
  * [Docker](#docker)
    * [Build](#build)
  * [Wrapplay commands](#wrapplay-commands)
<!-- TOC -->

---

A simple wrapper easy to configure and easy to dev (dev in few hours for save time)

License : GPL-2.0

Date : 02/2024

Stack : NodeJs, Playwright, NodeMailer

## Features

* read page
* compare content (with previous version)
* send notification mail if some difference was found

* Note :
    * this app do not need any database
    * dev. with playwright (only for fun)

## Quick start
* 1 - [Install app. for dev](#environmnet--dev)
* 2 - configure app. : ```./.env```
* 3 - Dev your wrappers on ```./src/config/www-index.config.ts```
* 4 - [Deploy on production](#environmnet--production)

Note : you can found a lot of exemple on ./src/config/www-resource.config.ts

## INSTALLATION

### ENVIRONMNET : DEV

* 1 - Init and configure your instance on ```./.env``` file : 
  ```bash 
  cp ./.env.sample ./.env
  ```

* 2 - Build your containers (without profil for production, or with "dev" profil). 
  ```bash
  docker-compose --project-name wrapplay --profile dev up --build -d
  ```

* 3 - Running app on dev. mode :
  ```bash
  npx playwright test --ui-port=9323 --ui-host=0.0.0.0
  ```

* 4 - Open you browser on :
  ```http://localhost:9323/```

* 5 - All your wrappers should be configured on :
  ```./src/config/www-index.config.ts```

### ENVIRONMNET : PRODUCTION

```bash
# install sources
cd /opt && git clone https://gitlab.com/wrapplay/wrapplay.git
chown -R <USER>:<GROUP> /opt/wrapplay && cd /opt/wrapplay

# instance configuration
cp .env.sample.prod .env
# change configuration on .env file

# if you need to login on gitlab repository or if you have error as "The provided password or token is incorrect or your account has 2FA enabled and you must use a personal access token"
# docker login registry.gitlab.com -u <LOGIN> -p <PERSONAL_TOKEN>
# now pull command should work :
# docker pull registry.gitlab.com/wrapplay/wrapplay/wrapplay:latest

# build environnement and prepare exec
docker-compose --project-name wrapplay up --build -d
docker exec -ti wrapplay npm run init
git config core.fileMode false && chmod +x ./bin/wrapplay.sh

# start manually :
/opt/wrapplay/bin/wrapplay.sh

# OR create crontab as (every 60min) :
*/60 * * * * /opt/wrapplay/bin/wrapplay.sh  >> /opt/wrapplay/data/logs/$(date +\%Y\%m\%d-\%H).log

```

* Tips :
  * ```kill -9 <PID>``` kill main service but not slaves services. For kill all process you can use : ```pkill -f playw``` 

## Docker

### Build

```bash
docker-compose --project-name wrapplay [--profile dev] up --build -d
```

## Wrapplay commands

```bash
# Runs all wrappers for DEV environmnt
npx playwright test

# Runs all wrappers for PRODUCTION environmnt
npm run start

# Starts the interactive UI mode for DEV environmnt
npm run start:dev

# Runs all wrappers for PRODUCTION environmnt and force html format
npm run start:html

# Runs only one wrapper by name
npx playwright test --reporter html -g "<JOB_NAME>"

# show reports
npm run show-report

# READ TRACES:
npx playwright show-trace <FILE_PATH>.zip
# OR use only tools : https://trace.playwright.dev/

# Auto generate tests with Codegen. (not work at this time)
# npx playwright codegen


```
