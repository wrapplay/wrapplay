import { defineConfig, devices } from '@playwright/test';
import * as path from 'path';
import { NotificationService } from './src/service/notification.service';
import { WrapperNotificationService } from './src/service/wrapper-notification.service';
import { EnvUtil } from './src/util/env.util';
import { ServerUtil } from './src/util/server.util';

/**
 * Read environment variables from file.
 * https://github.com/motdotla/dotenv
 */
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

global.appRoot = path.resolve(__dirname);

/**
 * This is app is so simple, we don't have any DI process.
 * So, simple singleton pattern is used.
 */
global.di = {
	notificationService: NotificationService.getInstance(),
	wrapperNotificationService: WrapperNotificationService.getInstance(NotificationService.getInstance()),
};

/**
 * See https://playwright.dev/docs/test-configuration.
 */
export default defineConfig({
	testDir: './src',
	testMatch: /.*\.process\.ts/,
	timeout: EnvUtil.getNumber('WRAPPER_TIMEOUT', 30000), // default=30000=30s
	/* Run tests in files in parallel */
	fullyParallel: true,
	/* Fail the build on CI if you accidentally left test.only in the source code. */
	forbidOnly: !!process.env.CI,
	retries: EnvUtil.getNumber('WRAPPER_RETRY', 0),
	/* Opt out of parallel tests on CI. */
	workers: EnvUtil.get('WRAPPER_THREAD', '1'),

	reporter: [
		// ['html', {
		// 	outputFolder: './data/test-reports',
		// 	outputDir: './data/test-results-reporter',
		// }
		// ],
		['./src/reporter/wrapplay.reporter.ts'],
	],

	outputDir: './data/test-results',

	/* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */
	use: {
		trace: 'retain-on-failure',
		locale: EnvUtil.get('BROWSER_LANG', 'fr-FR'),
		actionTimeout: EnvUtil.getNumber('WRAPPER_ACTION_TIMEOUT', 10000),
		navigationTimeout: EnvUtil.getNumber('WRAPPER_NAVIGATION_TIMEOUT', 20000),
	},

	/* Configure projects for major browsers */
	projects: [
		{
			name: 'chromium',
			use: {...devices['Desktop Chrome']},
		},
	],

});
