#!/bin/bash

#
# Simple bash script for used on Crontab
# This script pevent duplicate proces.
#

LOCK_FILE="/var/run/wrapplay.lock"

echo "[WRAPPLAY] - $(date +\%Y\%m\%d-\%H:\%M:\%S) - Stating..."

flock -n ${LOCK_FILE} docker exec wrapplay npm run start && echo "[WRAPPLAY] - $(date +\%Y\%m\%d-\%H:\%M:\%S) - End with success" || echo "[WRAPPLAY] - $(date +\%Y\%m\%d-\%H:\%M:\%S) - is lock due to another process"
