import { Page, test, TestInfo } from '@playwright/test';
import { WwwIndexConfig } from './config/www-index.config';
import { SimpleWrapper } from './wrapper/simple.wrapper';
import { ProcessWrapper } from './model/source.model';
import { DiffResponse } from './model/diff-response.model';
import { EnvUtil } from './util/env.util';
import { WrapperNotificationService } from './service/wrapper-notification.service';
import { extractDomain, slugifyUrl } from './util/browser.util';


const notificationService: WrapperNotificationService = global.di.wrapperNotificationService;

for (const source of WwwIndexConfig.SOURCES) {

	test.describe(source.name, (): void => {

		for (const proc of source.process) {

			const actions = proc.actions.map((action, idx) => ({idx, action}));
			for (const {idx, action} of actions) {

				for (const match of (source.matchs ?? [])) {

					const jobName = `${source.name}-${slugifyUrl(proc.url)}-${idx}-${match}`;

					test(jobName, async ({page}, testInfo): Promise<void> => {

						try {
							// open page on browser
							await goto(page, proc.url, match);

							// init wrapper
							action.init(source, proc, page);

							// hydrate wrapper instance
							if (action instanceof SimpleWrapper) {
								action.hydrateAdditionalMatch(match);
							}

							// process
							const response = await action.do();

							// attach screenshot on test result. usefull for dev mode
							if (response?.snapshot?.path) {
								await testInfo.attach(
									(response as DiffResponse).snapshot.filename,
									{path: (response as DiffResponse).snapshot.path}
								);
							}

							// notify each jobs
							await notificationService.sendEndJob(source, proc);

						} catch (e) {
							throw await handleJobException(page, proc, testInfo, jobName, e);
						}

					});

				}

			}

		}

	});

}

/**
 * Catch, log and return exception.
 */
const handleJobException = async (page: Page, proc: ProcessWrapper, testInfo: TestInfo, jobName: string, e: any): Promise<any> => {
	console.error(`[ERROR] - [${jobName}] - Job fail : `, e);
	if (EnvUtil.isTrue('SCREENSHOT_ON_ERROR')) {
		try {
			const path = `${global.appRoot}/data/errors/${jobName}.png`;
			await page.locator('body').screenshot({path});
			await testInfo.attach(`error::${path}`, {path});
		} catch (ee) {
			console.error(`[ERROR] - [${jobName}] - Fail to take ERROR screenshot : ` + ee?.message);
		}
	}

	try {
		await notificationService.sendJobError(proc, e);
	} catch (eee) {
		console.error(`[ERROR] - [${jobName}] - Fail to send ERROR notifiction : ` + eee?.message);
	}
	return e;
};

/**
 * Navigate on page.
 */
const goto = async (page: Page, urlTo: string, match: string | null = null): Promise<void> => {
	const matchEncoded = urlTo.includes('{MATCH}') && match ? encodeURI(match) : '';
	const url = matchEncoded ? urlTo.replace(/{MATCH}/g, matchEncoded) : urlTo;

	try {
		await page.goto(url, {timeout: EnvUtil.getNumber('WRAPPER_TIMEOUT', 0)});
	} catch (e) {
		console.log('[BUG] - retry goto function @see https://github.com/microsoft/playwright/issues/12182');
		await page.goto(url);
	}
};
