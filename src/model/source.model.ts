import { AbstractWrapper } from '../wrapper/abstract.wrapper';

export type ProcessWrapper = {
	/** auth page */
	url: string,
	actions: AbstractWrapper[],
};

/**
 * Configuration for wrap page
 */
export interface SourceModel {

	/** unique name / reference */
	name: string;

	/** actions, tests, to process */
	process: ProcessWrapper[];

	/** searched words (concat on Wrapper's 'matchs' attributs) */
	matchs?: string[];

}
