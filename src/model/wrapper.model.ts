import { Locator, Page } from '@playwright/test';
import { AbstractWrapperConfig } from '../wrapper/abstract.wrapper';

/**
 * Simple string Locator selector : @see https://playwright.dev/docs/api/class-selectors
 *
 * OR
 *
 * Complex/Custom Locator function : @see https://playwright.dev/docs/api/class-locator
 */
export type WrapperLocator = string | WrapperFunLocator;

/**
 * Wrapper used a custom callback function for get target locator.
 */
export interface WrapperFunLocator {
	/** unique if for stock data, generate file etc. */
	id: string;
	/** Callback function with Playwright object */
	locator: (page: Page, config: AbstractWrapperConfig) => Promise<Locator>;
}

/**
 * Wrapper configuration..
 */
export interface SimpleWrapperConfig extends AbstractWrapperConfig {
	/** searched word */
	match?: string | null;
	/** text selector for remove cookie banner */
	cookie?: string | null;
}
