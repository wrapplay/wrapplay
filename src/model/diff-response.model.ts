import { SnapshotResponse } from './snapshot.model';

export interface DiffResponse {
	/** Content have some change between previous release and current release*/
	haveChange: boolean;

	/** full content */
	content: string;

	/** added content (first diff founded between previous release and current release) */
	addedChange: string | null;

	/** searched terms */
	match: string,

	/** snapshot path if option is enable */
	snapshot?: SnapshotResponse | null,
}
