export interface SnapshotResponse {
	/** full path */
	path: string;
	/** filename */
	filename: string;
}
