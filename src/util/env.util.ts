/**
 * Helper for environment variables.
 */
export class EnvUtil {

	public static get(name: string, defaultValue: string|null = null): string | null {
		return process.env[name] ?? defaultValue;
	}

	public static getNumber(name: string, defaultValue: number|null = null): number | null {
		const r = +this.get(name);
		return !r && 0 !== r ? defaultValue : r;
	}

	public static getArray(name: string, defaultValue: string[]|null = null, delimitor: string = ','): string[] | null {
		return this.get(name)?.split(delimitor) ?? defaultValue;
	}

	public static isTrue(name: string): boolean {
		return ['true', true].includes(this.get(name));
	}

	public static isFalse(name: string): boolean {
		return !this.isTrue(name);
	}
}
