import { cpus, freemem, loadavg, totalmem } from 'os';
import { NumberUtil } from './number.util';
import { readdirSync, statSync } from 'fs';
import { join } from 'path';

export class ServerUtil {

	static BYTE_TO_MO = 1024 * 1024;

	/**
	 * Get AVG CPU usage on percentage (between 1 minute to 15 minutes).
	 */
	static cpuUsage(): number {
		return NumberUtil.round2Decimal(
			((loadavg().reduce((a, b) => a + b, 0)) / 3) * 100
		);
	}

	static cpuInfo(): string {
		const cpuInfos = cpus();
		return `${this.cpuUsage()}% (${cpus.length}x ${cpuInfos[0].model})`;
	}

	/**
	 * Get free memory on Mo.
	 */
	static memFree(): number {
		return NumberUtil.round2Decimal(freemem() / this.BYTE_TO_MO);
	}

	/**
	 * total available ram on Mo.
	 */
	static memTotal(): number {
		return NumberUtil.round2Decimal(totalmem() / this.BYTE_TO_MO);
	}

	/**
	 * usage on percentage
	 */
	static memUsage(): number {
		return NumberUtil.round2Decimal((this.memFree() / this.memTotal()) * 100);
	}

	static memInfo(): string {
		return `${this.memFree()} / ${this.memTotal()}Mo (${this.memUsage()}%)`;
	}

	/**
	 * Get folder size
	 * @param dir folder path (abs)
	 * @return Mo
	 */
	static dirSize(dir: string): number {
		return NumberUtil.round2Decimal(this.dirSizeAsByte(dir) / this.BYTE_TO_MO);
	}

	static dirSizeAsByte(dir: string): number {
		const files = readdirSync(dir, {withFileTypes: true});

		const paths = files.map(file => {
			const path = join(dir, file.name);

			if (file.isDirectory()) {
				return this.dirSizeAsByte(path);
			}

			if (file.isFile()) {
				const {size} = statSync(path);
				return size;
			}

			return 0;
		});

		return paths.flat(Infinity).reduce((i, size) => i + size, 0);
	}
}

