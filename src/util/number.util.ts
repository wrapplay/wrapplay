export class NumberUtil {

	static round2Decimal(decimal: number): number {
		return Math.round(decimal * 100) / 100;
	}

}
