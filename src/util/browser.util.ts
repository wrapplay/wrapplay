import { Locator, Page } from '@playwright/test';
import { NodeUtil } from './node.util';

/**
 * Execture ONE or MANY action on all founded nodes.
 */
export const forEachNode = async (locator: Locator, action: (locator: Locator) => Promise<any>, onlyVisible = true): Promise<void> => {
	const count = await locator.count();
	for (let i = 0; i < count; i++) {
		const childLocator = locator.nth(i);
		if (onlyVisible) {
			if (await childLocator.isVisible()) {
				await action(locator.nth(i));
			}
		} else {
			await action(locator.nth(i));
		}
	}
};

/**
 * Accept cookies.
 *
 * @param page to wrap
 * @param selector to click : accept/refused button for exemple
 * @param waitForHideSelector : selector for wait hidden action. popup modal for exemple
 */
export const consentCookies = async (page: Page, selector: string|null|undefined, waitForHideSelector: string|null = null): Promise<void> => {
	if (!selector) {
		return ;
	}

	// prevent delay on script load
	await page.waitForLoadState('domcontentloaded');
	await NodeUtil.sleep(500);

	const selectorText = selector.startsWith('#') || selector.startsWith('.') ? selector : `*:text("${selector}")`;

	// iterate on all founded node and try to click on each
	await forEachNode(page.locator(selectorText), async (item: Locator) => {
		await item.click();
		// prevent any script animation
		await NodeUtil.sleep(500);
	});

	// wait container was closed
	if (waitForHideSelector) {
		if (await page.isVisible(waitForHideSelector)) {
			await page.locator(waitForHideSelector).waitFor({state: 'hidden'});
		}
	}
};

/**
 * scroll down with mouse and keyboard
 */
export const scrollDown = async (page: Page): Promise<void> => {
	await page.locator('body').press('End');
	const scrollY = await page.evaluate(() => document.body.scrollHeight);
	await page.mouse.wheel(0, scrollY); // scroll to the bottom
};

/**
 * Extract Domain from URL.
 */
export const extractDomain = (url: string): string => (new URL(url)).hostname.replace('www.', '');

export const slugifyUrl = (url: string): string => url
	.replace(/\//g, '')
	.replace(/:/g, '')
	.replace(/ /g, '--')
	.replace(/"/g, '--')
	.replace(/'/g, '--')
	.replace(/\?/g, '--')
	.replace(/&/g, '--')
;
