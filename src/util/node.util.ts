export class NodeUtil {
	private static sleepSetTimeoutInstance = null;

	public static sleep(ms: number = 1000): Promise<void> {
		if (this.sleepSetTimeoutInstance) {
			clearInterval(this.sleepSetTimeoutInstance);
		}
		return new Promise(resolve => this.sleepSetTimeoutInstance = setTimeout(resolve, ms));
	}

}
