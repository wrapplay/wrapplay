export class DateUtil {
	static convertMsToHM(milliseconds?: number): string {
		if (!milliseconds) {
			return 'UNKNOW';
		}

		let seconds = Math.floor(milliseconds / 1000);
		let minutes = Math.floor(seconds / 60);
		let hours = Math.floor(minutes / 60);

		seconds = seconds % 60;
		minutes = seconds >= 30 ? minutes + 1 : minutes;
		minutes = minutes % 60;
		hours = hours % 24;

		return `${DateUtil.padTo2Digits(hours)}:${DateUtil.padTo2Digits(minutes)}`;
	}

	private static padTo2Digits(num: number): string {
		return num.toString().padStart(2, '0');
	}
}
