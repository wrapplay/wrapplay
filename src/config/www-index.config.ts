import { SourceModel } from '../model/source.model';
import { SimpleWrapper } from '../wrapper/simple.wrapper';
import { GENERIC_RESSOURCES, GENERIC_SEARCH_RESSOURCES, IDB_MARINE } from './www-resource.config';

/**
 * Declare all your wrappers process here.
 *
 * @see www-index-generic.config.ts for more exemple.
 */
export abstract class WwwIndexConfig {

	public static SOURCES: SourceModel[] = [
		// ########################### test ###########################
		{
			name: 'TEST',
			matchs: ['first27'],
			process: [
				{
					url: 'https://sigogneau.fr/op/test.html',
					actions: [new SimpleWrapper('body')],
				}
			],
		},
		// ########################### OVNI 28 ###########################
		{
			// arbitrary name (searchg category for exemple)
			name: 'OVNI28-words',
			// keyword to search and match on page
			matchs: ['ovni28', 'ovni 28'],
			// Wrapper process
			process: [
				...GENERIC_RESSOURCES,
				// sample : specific site
				{
					// page url to visit
					url: 'https://www.ovniclub.com/forum/ovni-vendre-f23.html',
					// actions to do on this page
					actions: [
						// Wrapper can do any action and wrap any keyword on page
						new SimpleWrapper('.topiclist.topics >> nth=1'),
					]
				},
			],
		},
		{
			name: 'OVNI28-searchs',
			matchs: ['ovni28 à vendre', 'ovni 28 à vendre', 'ovni28 occasion'],
			process: [
				...GENERIC_SEARCH_RESSOURCES,
			],
		},
		// ########################### FIRST 27.7 ###########################
		{
			name: 'FIRST27.7-words',
			matchs: ['first27', 'first 27'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'FIRST27.7-searchs',
			matchs: ['first27 à vendre', 'first27.7 à vendre', 'first27.7 occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### FIRST28 ###########################
		{
			name: 'FIRST28-words',
			matchs: ['first28', 'first 28'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'FIRST28-searchs',
			matchs: ['first 28 qr à vendre', 'first 28 qr occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### FIRST29 ###########################
		{
			name: 'FIRST29-words',
			matchs: ['first29', 'first 29'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'FIRST29-searchs',
			matchs: ['first 29 dl à vendre', 'first 29 dl occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### DEHLER29 ###########################
		{
			name: 'DEHLER29-words',
			matchs: ['dehler29', 'dehler 29'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'DEHLER29-searchs',
			matchs: ['dehler 29 à vendre', 'dehler 29 occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### Feeling30 ###########################
		{
			name: 'FEELING30-words',
			matchs: ['feeling30', 'feeling 30'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'FEELING30-searchs',
			matchs: ['feeling 30 di à vendre', 'feeling 30 di occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### RM800 ###########################
		{
			name: 'RM800-words',
			matchs: ['rm800', 'rm 800'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'RM800-searchs',
			matchs: ['rm800 à vendre', 'rm 800 à vendre', 'rm800 occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### DJANGO75 ###########################
		{
			name: 'DJANGO75-words',
			matchs: ['django75', 'django 75'],
			process: [
				...GENERIC_RESSOURCES,
				IDB_MARINE,
			],
		},
		{
			name: 'DJANGO75-searchs',
			matchs: ['django75 à vendre', 'django 75 à vendre'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### FILO ###########################
		{
			name: 'FILO-words',
			matchs: ['filo'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'FILO-searchs',
			matchs: ['filo à vendre', 'filo occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### MALANGO ###########################
		{
			name: 'MALANGO-words',
			matchs: ['malango'],
			process: [
				...GENERIC_RESSOURCES,
				IDB_MARINE,
			],
		},
		{
			name: 'MALANGO-searchs',
			matchs: ['malango à vendre', 'malango occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### BEPOX ###########################
		{
			name: 'BEPOX-words',
			matchs: ['bepox'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'BEPOX-searchs',
			matchs: ['bepox à vendre', 'bepox occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},
		// ########################### CRAFF29 ###########################
		{
			name: 'CRAFF29-words',
			matchs: ['craff29', 'craff 29'],
			process: GENERIC_RESSOURCES,
		},
		{
			name: 'CRAFF29-searchs',
			matchs: ['craff 29 à vendre', 'craff 29 occasion'],
			process: GENERIC_SEARCH_RESSOURCES,
		},

		// felling 326
		// sun odyssé 29.2
		// oceanis 281
	];

}
