import { ProcessWrapper } from '../model/source.model';
import { SimpleWrapper } from '../wrapper/simple.wrapper';
import { Locator, Page } from '@playwright/test';
import { consentCookies, forEachNode, scrollDown } from '../util/browser.util';
import { SimpleWrapperConfig } from '../model/wrapper.model';

export const HISSE_ET_HO: ProcessWrapper = {
	url: 'https://www.hisse-et-oh.com/av/sailboats',
	actions: [
		// SAMPLE : simple exemple with cookie banner to consent before read page
		new SimpleWrapper('#row-result', {cookie: 'Tout accepter et continuer'}),
	],
};

export const MILLE_SABORDS: ProcessWrapper = {
	url: 'https://www.lemillesabords.com/annonce-bateau-voilier-occasion',
	actions: [
		// SAMPLE : complex process with a searched form to fill and submit
		new SimpleWrapper({
			id: 'MILLE_SABORDS-row-result',
			async locator(page: Page, config: SimpleWrapperConfig): Promise<Locator> {
				// hydrate search form
				await forEachNode(page.locator('input[type="checkbox"][name="searchVoilier"]'), (item) => item.check(), false);
				await forEachNode(page.locator('input[type="checkbox"][name="searchMoteur"]'), (item) => item.uncheck(), false);
				await forEachNode(page.locator('[name="searchLongueur"]'), (item) => item.selectOption('8,10', {force: true}), false);
				await forEachNode(page.locator('input[type="text"][name="recherche_libre"]'), (item) => item.fill(config.match, {force: true}), false);

				// submit form
				await page.locator('button[type="button"][name="searchSubmit"] >> visible = true').first().click();

				// load all page
				await page.waitForURL('**/annonce-bateau**');
				await scrollDown(page);
				await page.waitForLoadState('domcontentloaded');

				// return result container
				return page.locator('[class="container-bids"]').first();
			}
		}, {cookie: 'J\'accepte'}),
	],
};

export const GOOGLE: ProcessWrapper = {
	// SAMPLE : use match term on URL
	url: 'https://www.google.com/search?q={MATCH}',
	actions: [
		new SimpleWrapper('#res', {cookie: 'Tout refuser'}),
	],
};

export const MAHE_NAUTUC: ProcessWrapper = {
	url: 'https://www.mahe-nautic.fr/liste-bateau.html?idm=&idmo=&prixmin=20000&prixmax=50000',
	actions: [
		new SimpleWrapper('#content'),
	],
};

export const ARNAUD_BAREY: ProcessWrapper = {
	url: 'https://arnaudbareyreyachting.com/voiliers-occasion/',
	actions: [
		new SimpleWrapper('[data-listing-source="posts"]'),
	],
};

export const ATLANTIQUE_YACHT: ProcessWrapper = {
	url: 'https://www.atlantique-yacht-broker.fr/voilier-d-occasion-a-vendre-morbihan-bretagne-bateaux2-3.html',
	actions: [
		new SimpleWrapper({
			id: 'ATLANTIQUE_YACHT-row-result',
			async locator(page: Page): Promise<Locator> {
				// sample usage with iframe on page
				const iframe = page.frameLocator('iframe');

				await iframe.locator('#etat').selectOption('1', {force: true});
				await iframe.locator('#price-input').fill('21000;50000', {force: true});
				await iframe.locator('#buttonSub').click();

				return iframe.locator('#properties');
			}
		}),
	],
};

export const RC_MARINE: ProcessWrapper = {
	url: 'https://www.rc-marine.fr/fr/stock-bateau?category=sailboat&brand=&price=2#result-boat',
	actions: [
		new SimpleWrapper('#result-boat'),
	],
};

export const CAP_OCEAN: ProcessWrapper = {
	// SAMPLE : use match term on URL
	url: 'https://www.cap-ocean.fr/?pfsearch-filter=date&pfsearch-filter-order=ASC&pfsearch-filter-number=5&pfsearch-filter-col=grid4&vkeywordss={MATCH}&fieldtype=37&longueur=7%2C10&annee=1950%2C2023&vprices=20000%2C50000&serialized=1&action=pfs&s=',
	actions: [
		new SimpleWrapper('form'),
	],
};

export const ADN_YACHT: ProcessWrapper = {
	url: 'https://www.adn-yachts.com/liste-voilier-occasion.html?m=&prixmin=20000&prixmax=50000',
	actions: [
		new SimpleWrapper('#content', {cookie: 'J\'accepte'}),
	],
};

export const ROYAL_NAUTISME: ProcessWrapper = {
	url: 'https://www.royal-nautisme.fr/liste-bateau-occasion.html?idm=&idmo=&lgmin=7&lgmax=10&prixmin=20000&prixmax=50000',
	actions: [
		new SimpleWrapper('.eshop-section', {cookie: 'J\'accepte'}),
	],
};

export const YOU_BOAT: ProcessWrapper = {
	url: 'https://www.youboat.com/fr/kw/bateaux?q={MATCH}',
	actions: [
		new SimpleWrapper('#lista', {cookie: 'Continuer sans accepter'}),
	],
};


export const PRO_ANNONCE: ProcessWrapper = {
	url: 'https://pro.annonces-marine.com/index.php?page=liste&idcli=2119&strcli=royal-nautisme-port-la-foret&etatads=1,2&t=1,2,3&listCat=&etat=1&listMarque=&PrixMin=20000&listModele=&PrixMax=50000&rechercher=Lancer%20la%20recherche&pg=1',
	actions: [
		new SimpleWrapper('#Listing'),
	],
};

export const IDB_MARINE: ProcessWrapper = {
	url: 'https://www.idbmarine.fr/fr/occasions.php',
	actions: [
		new SimpleWrapper('main'),
	],
};

export const BRETAGNE_YACHTING: ProcessWrapper = {
	url: 'https://www.bretagne-yachting.com/bateaux-d-occasions/nos-bateaux/',
	actions: [
		new SimpleWrapper('.content.boxed.grid2'),
	],
};

export const MARINE_WEST: ProcessWrapper = {
	url: 'https://www.marine-west.com/13-bateau-occasion?q=Type+de+bateau-Voile/Budget-de+20+%C3%A0+50.000%E2%82%AC',
	actions: [
		new SimpleWrapper('#js-product-list'),
	],
};

export const INTENSIVE_YACHTING: ProcessWrapper = {
	url: 'https://www.intensive-yachting.fr/vente-bateaux/',
	actions: [
		new SimpleWrapper('#et-boc'),
	],
};

export const CHANTIER_REDO: ProcessWrapper = {
	url: 'https://www.chantier-du-redo.com/vente-bateaux-d-occasions-morbihan-bretagne-46.html',
	actions: [
		new SimpleWrapper('#portfolio'),
	],
};

export const PLAISANCE_NAUTIQUE: ProcessWrapper = {
	url: 'https://www.plaisancenauticservices.fr/bateaux-occasion',
	actions: [
		new SimpleWrapper('.listeBateaux'),
	],
};

export const INFINITY_NAUTIQUE: ProcessWrapper = {
	url: 'https://infinitynautique.com/vente/occasion/',
	actions: [
		new SimpleWrapper('.et_pb_column.et_pb_column_4_4.et_pb_column_8_tb_body.et_pb_css_mix_blend_mode_passthrough.et-last-child'),
	],
};

export const PIRIAC_NAUTIC: ProcessWrapper = {
	url: 'https://www.piriac-nautic.com/liste-voilier-occasion.html',
	actions: [
		new SimpleWrapper('.eshop-section.section-white'),
	],
};

export const YUNIBOAT: ProcessWrapper = {
	url: 'https://www.yuniboat.com/liste-bateau.html?idm=&idmo=&prixmin=20000&prixmax=50000',
	actions: [
		new SimpleWrapper('.products'),
	],
};

export const WEST_YACHTING: ProcessWrapper = {
	url: 'https://www.west-yachting.com/voilier-occasion-vannes-arzon-morbihan.html',
	actions: [
		new SimpleWrapper('#boatlist', {cookie: '✓ OK, tout accepter'}),
	],
};

export const AD56_ARMORIQUE_DIFFUSION: ProcessWrapper = {
	// AD56.fr use iframe on this site
	url: 'https://io.youboat.com/ad-marine/voiliers?rwcli=ad-marine&idt=2&etat=&idc=&idm=&price=20000%3B50000',
	actions: [
		new SimpleWrapper('#properties'),
	],
};

export const CHANTIER_MARITIME_DU_CROUESTY: ProcessWrapper = {
	url: 'https://www.chantiermaritimeducrouesty.fr/liste-bateau-occasion.html?idm=&idmo=&lgmin=7&lgmax=10&prixmin=20000&prixmax=50000',
	actions: [
		new SimpleWrapper('.products', {cookie: 'J\'accepte'}),
	],
};

export const NAUTIC_44: ProcessWrapper = {
	url: 'https://www.44nautic.fr/liste-voilier-occasion.html',
	actions: [
		new SimpleWrapper('#container', {cookie: 'J\'accepte'}),
	],
};

export const CAP_A_LOUEST: ProcessWrapper = {
	url: 'https://www.cap-a-louest.fr/resultats-recherche.php',
	actions: [
		new SimpleWrapper({
			id: 'CAP_A_LOUEST',
			async locator(page: Page): Promise<Locator> {
				await consentCookies(page, 'Tout accepter', '#tarteaucitronAlertBig');
				const searchDiv = (await page.locator('#bl-se').isVisible({timeout: 2500})) ? page.locator('#bl-se') : page.locator('#bl-se2');
				await searchDiv.getByRole('radio').first().click();
				await searchDiv.locator('#se-submit').click();
				await page.waitForURL('**/resultats-recherche**');

				return page.locator('#content');
			}
		}),
	],
};

export const BLEU_RIVAGE: ProcessWrapper = {
	url: 'https://www.bleurivage.com/vente-bateaux-occasion/voilier/',
	actions: [
		new SimpleWrapper('.products'),
	],
};

export const LOCA_VOILE: ProcessWrapper = {
	url: 'https://www.lokavoile.fr/occasion-voiliers/',
	actions: [
		new SimpleWrapper('.l-main', {cookie: '#cn-accept-cookie'}),
	],
};


export const ANTIPODE_YACHTS: ProcessWrapper = {
	url: 'https://www.antipode-yachts.com/fr/recherche.html',
	actions: [
		new SimpleWrapper({
			id: 'ANTIPODE_YACHTS-row-result',
			async locator(page: Page, config: SimpleWrapperConfig): Promise<Locator> {
				await consentCookies(page, 'Accepter');

				const searchContainer = page.locator('#EmbeddedSearchEngine');
				await searchContainer.locator('#Type span').first().click();
				await searchContainer.locator('[name="longueur"]').selectOption('1', {force: true});
				await searchContainer.locator('[name="prix"]').selectOption('1', {force: true});
				await searchContainer.locator('[name="rechercher"]').click();

				await page.waitForURL('**/fr/recherche**');

				return page.locator('#Isotoped');
			}
		}, {cookie: 'J\'accepte'}),
	],
};

export const QUIBERON_NAUTIC: ProcessWrapper = {
	url: 'https://www.quiberon-nautic.com/categorie-produit/occasion/voiliers/',
	actions: [
		new SimpleWrapper('[role="main"]'),
	],
};

export const LA_BAULE_NAUTIC: ProcessWrapper = {
	url: 'https://www.labaulenautic.com/gamme/voiliers-doccasion/',
	actions: [
		new SimpleWrapper('.wrapper-archives-bateaux-inner', {cookie: 'Tout accepter'}),
	],
};

const BOAT_DIFF_WRAP = (): SimpleWrapper => new SimpleWrapper({
		id: 'BOAT_DIFF_44',
		async locator(page: Page, config: SimpleWrapperConfig): Promise<Locator> {
			const r = page.locator('#main');
			await r.waitFor({state: 'visible'});
			return r;
		}
	}, {cookie: 'J\'accepte'});

export const BOAT_DIFF_44: ProcessWrapper = {
	url: 'https://www.boats-diffusion.com/votre-recherche/?keyword=&type=voilier&status=any&longueur=any&max-longueur=+10&largeur=any&min-price=20000&max-price=50000&place=pornic-44',
	actions: [
		BOAT_DIFF_WRAP()
	],
};

export const BOAT_DIFF_17: ProcessWrapper = {
	url: 'https://www.boats-diffusion.com/votre-recherche/?keyword=&type=voilier&status=any&longueur=any&max-longueur=+10&largeur=any&min-price=20000&max-price=50000&place=la-rochelle-17',
	actions: [
		BOAT_DIFF_WRAP()
	],
};

export const BOAT_DIFF_29: ProcessWrapper = {
	url: 'https://www.boats-diffusion.com/votre-recherche/?keyword=&type=voilier&status=any&longueur=any&max-longueur=+10&largeur=any&min-price=20000&max-price=50000&place=benodet-29',
	actions: [
		BOAT_DIFF_WRAP()
	],
};

// cannot : captcha challenge
// export const ANNONCES_BATEAU: ProcessWrapper = {
// 	url: 'https://www.annoncesbateau.com/bateaux/categorie-voiliers/pays-france/longueur-7,10/prix-20000,50000/trier-modifie:decroiss/',
// 	actions: [
// 		new SimpleWrapper('.search-results', {cookie: 'Refuser tout'}),
// 	],
// };

/**
 * Generic sites
 */
export const GENERIC_RESSOURCES: ProcessWrapper[] = [
	HISSE_ET_HO,
	MILLE_SABORDS,
	MAHE_NAUTUC,
	ARNAUD_BAREY,
	ATLANTIQUE_YACHT,
	RC_MARINE,
	CAP_OCEAN,
	ADN_YACHT,
	ROYAL_NAUTISME,
	YOU_BOAT,
	PRO_ANNONCE,
	BRETAGNE_YACHTING,
	MARINE_WEST,
	INTENSIVE_YACHTING,
	CHANTIER_REDO,
	PLAISANCE_NAUTIQUE,
	INFINITY_NAUTIQUE,
	PIRIAC_NAUTIC,
	YUNIBOAT,
	WEST_YACHTING,
	AD56_ARMORIQUE_DIFFUSION,
	CHANTIER_MARITIME_DU_CROUESTY,
	NAUTIC_44,
	CAP_A_LOUEST,
	BLEU_RIVAGE,
	LOCA_VOILE,
	ANTIPODE_YACHTS,
	QUIBERON_NAUTIC,
	LA_BAULE_NAUTIC,
	BOAT_DIFF_44,
	BOAT_DIFF_17,
	BOAT_DIFF_29,
];

/**
 * Search engines
 */
export const GENERIC_SEARCH_RESSOURCES: ProcessWrapper[] = [
	GOOGLE,
];
