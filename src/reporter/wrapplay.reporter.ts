import { FullConfig, FullResult, Reporter, Suite, TestCase, TestResult } from 'playwright/types/testReporter';
import { DateUtil } from '../util/date.util';
import { TestStatus } from 'playwright/types/test';
import { NotificationService } from '../service/notification.service';
import { EnvUtil } from '../util/env.util';
import * as os from 'os';
import { ServerUtil } from '../util/server.util';

export interface Trace {
	date: string;
	test: string;
	attachments?: string[];
	error: string;
}

export default class WrapplayReporter implements Reporter {

	/** number of wrapper */
	static TOTAL: number = 0;

	/** start itearation date */
	static START: string | null = null;

	/** count number of worker */
	static WORKER: number = 0;

	/** count wrapper's results */
	static RESULT: { [P in TestStatus]: number } = {
		passed: 0,
		failed: 0,
		skipped: 0,
		interrupted: 0,
		timedOut: 0,
	};

	static TRACES: Omit<{ [P in TestStatus]: Trace[] }, 'passed'> = {
		failed: [],
		skipped: [],
		interrupted: [],
		timedOut: [],
	};

	onBegin(config: FullConfig, suite: Suite): void {
		WrapplayReporter.START = this.nowToLocaleDate();
		WrapplayReporter.TOTAL = suite.allTests().length;
		WrapplayReporter.WORKER = config.workers;

		this.log(`[START] - Running ${WrapplayReporter.TOTAL} wrappers on ${WrapplayReporter.WORKER} workers`);
	}

	/**
	 * On each job start.
	 */
	onTestBegin(test: TestCase, result: TestResult): void {
		if (EnvUtil.isTrue('WRAPPER_LOG_START')) {
			this.log(`Starting ${test.title}`);
		}
	}

	/**
	 * On each job end.
	 */
	onTestEnd(test: TestCase, result: TestResult): void {
		// count test by status
		++WrapplayReporter.RESULT[result.status];

		const haveError = 'passed' !== result.status;
		let error: string|null = null;

		// track errors
		if (haveError) {
			error = result.error?.message ?? 'UNKNOW';
			WrapplayReporter.TRACES[result.status].push({
				date: this.nowToLocaleDate(),
				test: test.title,
				attachments: result.attachments
					.filter(item => item.name?.startsWith('error::'))
					// keep name only. path if modified by playwright and is not the same as unix path
					.map(item => item.name.replace('error::', '')),
				error,
			});
		}

		this.log(`${this.countProcessed()}/${WrapplayReporter.TOTAL} - [${haveError ? 'ERROR' : 'SUCCESS'}] - ${test.title} : ${result.status.toUpperCase()} in ${result.duration}ms ${error ? 'ERROR=' + error : ''}`);

		if (EnvUtil.isTrue('WRAPPER_LOG_STACKTRACE') && result.error?.stack) {
			console.error(result.error?.stack);
		}
	}

	async onEnd(result: FullResult): Promise<void> {
		const success = WrapplayReporter.RESULT.passed;
		const error = this.countErrors();
		const timeout = WrapplayReporter.RESULT.timedOut;

		this.log(`[END] - Duration : ${DateUtil.convertMsToHM(result.duration)} 
				            Success : ${success}/${WrapplayReporter.TOTAL}
				            Error : ${error}
				            Timeout : ${timeout}. 
				            Status=${result.status}
		`);

		await this.sendEndReport(result);
	}

	private async sendEndReport(result: FullResult): Promise<void> {
		if (EnvUtil.isTrue('NOTIFY_END_JOBS')) {
			const recipients = EnvUtil.get('RECIPIENTS_MONITOR');
			if (!recipients) {
				return ;
			}

			const service: NotificationService = global.di.notificationService;

			let errors = '';

			// eslint-disable-next-line guard-for-in
			for (const key in WrapplayReporter.TRACES) {
				const traces: Trace[] | null = WrapplayReporter.TRACES[key] ?? null;
				if (traces?.[0]) {
					errors += `<small><strong>${key.toUpperCase()}</strong></small>`;
					for (const trace of traces) {
						errors += `<hr>
									<ul>
										<li>DATE : ${trace.date}</li>
										<li>WRAPPER : ${trace.test}</li>
										<li>ERROR : ${trace.error}</li>`;

						if (EnvUtil.get('SCREENSHOT_ON_ERROR') && trace.attachments?.[0]) {
							errors += `<li>SCREENSHOT : <br>${trace.attachments.join('<br>')}</li>`;
						}

						errors += `</ul>`;
					}
				}
			}

			await service.send(
				'Wrapplay - Report',
				{
					html: `
					<strong>CPU : </strong>${ServerUtil.cpuInfo()}<br>
					<strong>RAM : </strong>${ServerUtil.memInfo()}<br>
					<strong>Wrapplay DATA : </strong>${ServerUtil.dirSize(global.appRoot + '/data')}Mo<br><br>

					<strong>Start</strong> : ${WrapplayReporter.START}<br>
					<strong>End</strong> : ${this.nowToLocaleDate()}<br>
					<strong>Worker</strong> : ${WrapplayReporter.WORKER}<br>
					<strong>Duration</strong> : ${DateUtil.convertMsToHM(result.duration)}<br><br>
					
					<strong>Success</strong> : ${WrapplayReporter.RESULT.passed} / ${WrapplayReporter.TOTAL}<br>
					<strong>Timeout</strong> : ${WrapplayReporter.RESULT.timedOut}<br>
					<strong>Errors</strong> : ${this.countErrors()}<br><br>
					
					${errors ? '<strong>Details</strong> : <br><br>' : ''}
					${errors}
					`,
				},
				[],
				recipients
			);
		}
	}

	/**
	 * Count number of ended jobs.
	 * @private
	 */
	private countProcessed(): number {
		let doneCount = 0;
		// eslint-disable-next-line guard-for-in
		for (const key in WrapplayReporter.RESULT) {
			doneCount += WrapplayReporter.RESULT[key];
		}
		return doneCount;
	}

	private countErrors(): number {
		return WrapplayReporter.RESULT.failed
			+ WrapplayReporter.RESULT.skipped
			+ WrapplayReporter.RESULT.interrupted;
	}

	private log(message: string): void {
		const now = this.nowToLocaleDate();
		console.log(`[WRAPPPLAY] - ${now} - ${message}`);
	}

	private nowToLocaleDate(): string {
		const date = new Date();
		const lang = EnvUtil.get('BROWSER_LANG');
		return date.toLocaleDateString(lang) + ' ' + date.toLocaleTimeString(lang);
	}
}

