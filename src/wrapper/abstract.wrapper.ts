import { Locator, Page } from '@playwright/test';
import { WrapperLocator } from '../model/wrapper.model';
import { ProcessWrapper, SourceModel } from '../model/source.model';
import { extractDomain } from '../util/browser.util';

/**
 * Wrapper configuration.
 */
export interface AbstractWrapperConfig {}

export abstract class AbstractWrapper {

	protected source?: SourceModel;
	protected proc?: ProcessWrapper;
	protected page?: Page;

	protected constructor(
		public locator: WrapperLocator,
		public config: AbstractWrapperConfig|null = null,
	) {
	}

	public init(source: SourceModel, proc: ProcessWrapper, page: Page): this {
		this.source = source;
		this.proc = proc;
		this.page = page;
		return this;
	}

	/**
	 * Exec. business code.
	 */
	public abstract do(): Promise<any>;

	/**
	 * helper for get you Locator object configured on @see www-index.constant.ts
	 */
	public async getLocator(): Promise<Locator> {
		await this.page.waitForLoadState('domcontentloaded');
		if ('string' === typeof this.locator) {
			return this.page.locator(this.locator).first();
		} else {
			return await this.locator.locator(this.page, this.config);
		}
	}

	/**
	 * Get unique id for generate uniq file, store data, etc.
	 */
	public getId(jobId: string): string {
		const domain = extractDomain(this.proc.url);
		const name = this.source.name.replace(/ /g, '-');

		const locator = ('string' === typeof this.locator ? this.locator : this.locator.id)
			// remove spaces
			.replace(/ /g, '--')
			// remove special char
			.replace(/[^a-zA-Z0-9 ]/g, 'X')
			;

		return `${domain}_${name}_${jobId ? '_' + jobId : ''}${locator}`;
	}

	/**
	 * Format/Compact content.
	 */
	protected formatContent(content: string): string {
		return content.replace(/(\r\n|\n|\r|\t)/gm, '');
	}
}
