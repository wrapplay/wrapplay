import * as fs from 'fs';
import { SimpleWrapperConfig, WrapperLocator } from '../model/wrapper.model';
import { SnapshotResponse } from '../model/snapshot.model';
import { DiffResponse } from '../model/diff-response.model';
import { consentCookies } from '../util/browser.util';
import { AbstractWrapper } from './abstract.wrapper';
import { WrapperNotificationService } from '../service/wrapper-notification.service';
import diff = require('fast-diff');
import { EnvUtil } from '../util/env.util';

/**
 * Wrapper for extract Diff (added) content on page.
 */
export class SimpleWrapper extends AbstractWrapper {

	public static STORAGE = '/data/db';

	protected wrapperService: WrapperNotificationService;

	constructor(
		public locator: WrapperLocator,
		/** matched terms */
		public config: SimpleWrapperConfig = null,
	) {
		super(locator, config);
		this.wrapperService = global.di.wrapperNotificationService;
	}

	/**
	 * Add additional "match term" on current wrapper.
	 */
	public hydrateAdditionalMatch(match: string): this {
		if (!this.config) {
			this.config = {};
		}
		this.config.match = match;
		return this;
	}

	async do(): Promise<DiffResponse> {
		// accept cookie popup
		await consentCookies(this.page, this.config?.cookie);

		// get added change on page
		const response = await this.buildChanges();

		if (response.haveChange) {
			console.log(`[${this.getId()}] - Change detected that match with : ${this.config.match}`);
			await this.wrapperService.sendChange(this.source, this.proc, response);
		} else {
			console.log(`[${this.getId()}] - no change detected `);
		}

		return response;
	}

	/**
	 * Compute changes (only added content).
	 */
	private async buildChanges(): Promise<DiffResponse> {
		const alwayTakeScreenShot = EnvUtil.isTrue('SCREENSHOT_ENABLED_ALWAYS');
		const locator = await this.getLocator();
		const content = this.formatContent(await locator.innerHTML());

		let haveChange = false;
		let addedChange: string | null = null;
		let snapshot: SnapshotResponse | null = null;

		if (fs.existsSync(this.getDbFile())) {
			const existingContent = this.formatContent(fs.readFileSync(this.getDbFile(), {encoding: 'utf8'}));

			// @see https://www.npmjs.com/package/fast-diff
			const d: diff.Diff[] = diff(existingContent, content);

			// extract only founded content
			addedChange = d.filter(item => 1 === item[0])?.[0]?.[1];

			if (addedChange) {
				// check search term
				if (addedChange.toLowerCase().includes(this.config.match.toLowerCase())) {
					haveChange = true;
					// take snapshot only if detected change match with keyword
					if (!alwayTakeScreenShot) {
						snapshot = await this.createScreenshot();
					}
				}
			}

		}

		if (alwayTakeScreenShot) {
			snapshot = await this.createScreenshot();
		}

		fs.writeFileSync(this.getDbFile(), content, {encoding: 'utf8'});

		return {
			haveChange,
			addedChange,
			content,
			match: this.config.match,
			snapshot,
		};
	}

	/**
	 * Create screenshot of current locator node.
	 */
	private async createScreenshot(): Promise<SnapshotResponse | null> {
		if (EnvUtil.isTrue('SCREENSHOT_ENABLED')) {
			const path = `${this.getDbFile()}.png`;
			const filename = `${this.getId()}.png`;
			await (await this.getLocator()).screenshot({path});
			return {path, filename};
		}
		return null;
	}

	/**
	 * Data is stored on plain-text file.
	 * This method return full path of this db file.
	 */
	public getDbFile(): string {
		return `${global.appRoot}${SimpleWrapper.STORAGE}/${this.getId()}`;
	}

	public getId(): string {
		const match = this.config.match.replace(/ /g, '--');
		return super.getId(match);
	}

}
