import * as nodemailer from 'nodemailer';
import { Transporter } from 'nodemailer';
import * as SMTPTransport from 'nodemailer/lib/smtp-transport';
import { Attachment } from 'nodemailer/lib/mailer';
import { EnvUtil } from '../util/env.util';

export interface NotificationContent {
	html?: string;
	text?: string;
}

/**
 * Notification service for send Email over SMTP transport.
 */
export class NotificationService {

	private static _instance?: NotificationService;

	private transporter!: Transporter<SMTPTransport.SentMessageInfo>;

	private recipients?: string | null = null;

	private constructor() {
	}

	static getInstance(): NotificationService {
		if (!this._instance) {
			this._instance = new NotificationService();
			if (this.isEnabled()) {
				this._instance.transporter = this.buildTransport();
				this._instance.recipients = EnvUtil.get('RECIPIENTS');
			}
		}

		return this._instance;
	}

	public static isEnabled(): boolean {
		return EnvUtil.isTrue('SMTP_ENABLED');
	}

	/**
	 * Build SMTP transport.
	 */
	private static buildTransport(): Transporter<SMTPTransport.SentMessageInfo> {
		const config: SMTPTransport | SMTPTransport.Options = {};

		if (EnvUtil.get('SMTP_SERVICE')) {
			config.service = EnvUtil.get('SMTP_SERVICE');
		} else {
			config.host = EnvUtil.get('SMTP_HOST');
			config.port = EnvUtil.getNumber('SMTP_PORT');
			config.secure = EnvUtil.isTrue('SMTP_SECURE');
		}

		const user = EnvUtil.get('SMTP_LOGIN');
		const pass = EnvUtil.get('SMTP_PASS');
		if (user && pass) {
			config.auth = {
				type: 'LOGIN',
				user,
				pass,
			};
		}

		return nodemailer.createTransport(config);
	}

	/**
	 * Send notification By Email.
	 *
	 * @param subject email subhect
	 * @param content email content as text and html format
	 * @param attachments email attachments
	 * @param recipients override default recipients
	 */
	public async send(subject: string, content: NotificationContent, attachments: Attachment[] = [], recipients: string | null = null): Promise<SMTPTransport.SentMessageInfo> {

		try {
			if (!NotificationService.isEnabled() || !this.recipients) {
				return;
			}

			if (!content.html) {
				content.html = content.text;
			}

			return await this.transporter.sendMail({
				from: EnvUtil.get('SMTP_FROM') ?? EnvUtil.get('SMTP_LOGIN') ?? 'wrapplay@mail.com',
				to: recipients ?? this.recipients,
				subject,
				attachments,
				...content,
			});
		} catch (e) {
			console.error(`[NOTIFICATION] - Fail to send email. Subject=${subject}, Recipients:${recipients} Attachments=${attachments.join(', ')} Message=${e?.message ?? 'unknow'} :`, e);
		}

	}

}
