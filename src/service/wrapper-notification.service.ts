import { ProcessWrapper, SourceModel } from '../model/source.model';
import { DiffResponse } from '../model/diff-response.model';
import { Attachment } from 'nodemailer/lib/mailer';
import sanitizeHtml from 'sanitize-html';
import { extractDomain } from '../util/browser.util';
import { NotificationContent, NotificationService } from './notification.service';
import { EnvUtil } from '../util/env.util';

export class WrapperNotificationService {

	private static _instance?: WrapperNotificationService;


	private constructor(
		protected notificationService: NotificationService,
	) {
	}

	static getInstance(notificationService: NotificationService): WrapperNotificationService {
		if (!this._instance) {
			this._instance = new WrapperNotificationService(notificationService);
		}

		return this._instance;
	}

	async sendChange(source: SourceModel, proc: ProcessWrapper, response: DiffResponse): Promise<void> {
		const {subject, content, attachments} = await this.buildNotification(source, proc, response);
		await this.notificationService.send(subject, content, attachments);

	}

	/**
	 * Log error and try to send notification for monitor.
	 *
	 * @param proc
	 * @param e
	 */
	async sendJobError(proc: ProcessWrapper, e: Error): Promise<void> {
		console.error(`Fail to process ${proc.url}. Error : ${e.message ?? ''}`, e);

		if (EnvUtil.isTrue('NOTIFY_ALL_JOBS')) {
			const recipients = EnvUtil.get('RECIPIENTS_MONITOR');
			if (recipients) {
				try {
					await this.notificationService.send(
						`Wrapplay - Exception "${e.name ?? 'Unknown'}"`,
						{
							text: `Application error ${e.name ?? ''} on ${proc.url} : ${e.message ?? ''} \n\n ${e.stack ?? ''}`,
							html: `Application error ${e.name ?? ''} on ${proc.url} : ${e.message ?? ''} \n\n ${e.stack ?? ''}`,
						},
						[],
						recipients);
				} catch (ee) {
					console.error(`Fail to monitor error : ${ee.message ?? ''}`, ee);
				}
			}
		}

	}

	/**
	 * Log Job's end.
	 *
	 * @param source
	 * @param proc
	 */
	async sendEndJob(source: SourceModel, proc: ProcessWrapper): Promise<void> {
		if (EnvUtil.isTrue('NOTIFY_ALL_JOBS')) {
			const ref = `${proc.url} - ${source.name}`;

			const recipients = EnvUtil.get('RECIPIENTS_MONITOR');
			if (recipients) {
				console.log(`[${ref}] - Send End notification to ${recipients}`);
				try {
					const content = `Job end with success : ${ref}`;
					await this.notificationService.send(
						`Wrapplay - Job Success  - ${ref}`,
						{
							text: content,
							html: content,
						},
						[],
						recipients);
				} catch (ee) {
					console.error(`[${ref}] - Fail to monitor EndJob. error : ${ee.message ?? ''}`, ee);
				}
			}
		}
	}

	/**
	 * Build notification objects.
	 */
	// eslint-disable-next-line @typescript-eslint/require-await
	async buildNotification(source: SourceModel, proc: ProcessWrapper, response: DiffResponse): Promise<{
		subject: string,
		content: NotificationContent,
		attachments: Attachment[]
	}> {
		const subject: string = this.buildSubject(`${source.name} - ${extractDomain(proc.url)}`);

		const content: NotificationContent = {
			text: this.buildText(proc, response),
			html: this.buildHtml(proc, response),
		};

		const attachments: Attachment[] = [];
		if (this.isSnapshotEnable && response.snapshot) {
			attachments.push({cid: response.snapshot.filename, ...response.snapshot});
		}

		return {subject, content, attachments};
	}

	/**
	 * Build notification Subject.
	 */
	buildSubject(subject: string): string {
		return 'Wrapplay :: ' + (EnvUtil.get('MAIL_SUBJECT', '')) + subject;
	}

	/**
	 * Build notification PlainText format.
	 */
	buildText(proc: ProcessWrapper, response: DiffResponse): string {
		return `
		Souce : 
		${proc.url}
		
		Founded : 
		${response.match}
		
		New Content :
		${sanitizeHtml(response.addedChange)}
	`;
	}

	/**
	 * Build notification Html format.
	 */
	buildHtml(proc: ProcessWrapper, response: DiffResponse): string {
		let res = `
		<strong>Souce : </strong> 
		<br>
		<a href="${proc.url}">${proc.url}</a>
		<br><br>
		
		<strong>Matchs words : </strong> 
		<br>
		${response.match}
		<br><br>
		
		<strong>New Content :</strong>
		<br>
		${sanitizeHtml(response.addedChange)}
		<br><br>
	`;

		if (this.isSnapshotEnable && response.snapshot) {
			res += `
			<strong>Snapshoot :</strong>
			<br>
			<img src="cid:${response.snapshot.filename}" alt="Snapshoot">
			<br><br>
		`;
		}

		return res;
	}

	get isSnapshotEnable(): boolean {
		return EnvUtil.isTrue('SCREENSHOT_ENABLED');
	}

}
